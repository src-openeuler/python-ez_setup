%global _empty_manifest_terminate_build 0
Name:		python-ez_setup
Version:	0.9
Release:	1
Summary:	ez_setup.py and distribute_setup.py
License:	MIT
URL:		http://github.com/ActiveState/ez_setup
Source0:	https://files.pythonhosted.org/packages/ba/2c/743df41bd6b3298706dfe91b0c7ecdc47f2dc1a3104abeb6e9aa4a45fa5d/ez_setup-0.9.tar.gz
BuildArch:	noarch


%description
**Problem**: ``setup.py`` of several Python projects blindly import the
setuptools bootstrap module ``ez_setup.py`` .this is
to remain compatible with several Python distributors opting to use Distribute
instead of Setuptools -- examples: Debian, ActiveState, and so on.

%package -n python3-ez_setup
Summary:	ez_setup.py and distribute_setup.py
Provides:	python-ez_setup
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
%description -n python3-ez_setup
**Problem**: ``setup.py`` of several Python projects blindly import the
setuptools bootstrap module ``ez_setup.py``.this is
to remain compatible with several Python distributors opting to use Distribute
instead of Setuptools -- examples: Debian, ActiveState, and so on.

%package help
Summary:	Development documents and examples for ez_setup
Provides:	python3-ez_setup-doc
%description help
**Problem**: ``setup.py`` of several Python projects blindly import the
setuptools bootstrap module ``ez_setup.py``.this is
to remain compatible with several Python distributors opting to use Distribute
instead of Setuptools -- examples: Debian, ActiveState, and so on.

%prep
%autosetup -n ez_setup-0.9

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-ez_setup -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Wed Sep 02 2020 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
